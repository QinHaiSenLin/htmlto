package com.xixudi.htmlto;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Engine;
import com.jfinal.template.Template;

import java.io.*;
import java.util.ArrayList;

/**
 * @Description 根据模板生成chm工程文件,继而生成chm
 * @Author YangZheng 328170112@qq.com
 * @Date 2019-02-18 10:25
 */
public class MakeChm {
    //chm相关文件路径
    private static String chmPath=MakeChm.class.getClassLoader().getResource("chm").getPath().substring(1);

    private String projectName;
    private String basePath;
    Kv kv;
    MakeChm(String projectName,ArrayList<ContentsNode> nodes,String indexPage,String title){
        this.projectName = projectName;
        this.basePath=MakeChm.class.getClassLoader().getResource(projectName).getPath().substring(1);
        if(StrKit.isBlank(indexPage)) indexPage = getFirstPage(nodes);
        if(StrKit.isBlank(title)) title = projectName+"帮助文档";
        this.kv = Kv.by("nodes", ContentsNode.makeTreeData(nodes)).set("projectName",projectName).set("index","source\\"+indexPage).set("title",title);
    }
    public void make() {
        writePropFile("hhc");
        writePropFile("hhk");
        writePropFile("hhp");
        makeChm();
    }
    public void makeIndexPage(){
        writePropFile("index.html","source/index.html","utf8");
        ArrayList<ContentsNode> nodes = (ArrayList<ContentsNode>)kv.get("nodes");
        nodes.add(0,ContentsNode.newTopic("速查表","index.html") );
    }
    private void makeChm() {
        String command = "\""+chmPath+"/hhc.exe\" "+ projectName +".hhp";
        System.out.println(command);
        try {
            Process p = Runtime.getRuntime().exec(command,null,new File(basePath));

            InputStream is = p.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            p.waitFor();
            if (p.exitValue() != 0) {
                //说明命令执行失败
                //可以进入到错误处理步骤中
            }
            String s = null;
            while ((s = reader.readLine()) != null) {
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void writePropFile(String suffix){
        writePropFile("main."+suffix, projectName +"."+suffix,"gbk");
    }
    private void writePropFile(String oldName,String newName,String charset){
        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(basePath+"/"+newName), charset));
            Template template = Engine.use().setBaseTemplatePath(chmPath).getTemplate(oldName);
            template.render(kv, output);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private static String getFirstPage(ArrayList<ContentsNode> nodes){
        for (ContentsNode node : nodes) {
            if(StrKit.isBlank(node.getPath())) {
                if(node.getChildren()!=null&&node.getChildren().size()>0) {
                    return getFirstPage(node.getChildren());
                } else {
                    return "";
                }
            } else {
                return node.getPath();
            }
        }
        return "";
    }
    public static void createChm(String name,ArrayList<ContentsNode> nodes,String indexPage,String title,boolean needMakeIndexPage){
        MakeChm makeChm = new MakeChm(name,nodes,indexPage,title);
        if(needMakeIndexPage) makeChm.makeIndexPage();
        makeChm.make();
    }
}
