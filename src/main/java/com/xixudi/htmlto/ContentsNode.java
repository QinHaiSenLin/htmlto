package com.xixudi.htmlto;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author YangZheng 328170112@qq.com
 * @Date 2019-02-18 9:49
 */
public class ContentsNode {
    private boolean isHeading;
    private String title;
    private String path;
    private ArrayList<ContentsNode> children;
    ContentsNode(String title,String path,boolean isHeading){
        this.title = title;
        this.path = path;
        this.isHeading = isHeading;
    }
    public static ContentsNode newHeading(String title){
        return newHeading(title,null);
    }
    public static ContentsNode newHeading(String title,String path){
        ContentsNode contentsNode = new ContentsNode(title, path, true);
        contentsNode.setChildren(new ArrayList<ContentsNode>());
        return contentsNode;
    }
    public static ContentsNode newTopic(String title,String path){
        return new ContentsNode(title,path,false);
    }

    public boolean addChild(ContentsNode node){
        if(isHeading && children!=null)
            return children.add(node);
        else
            return false;
    }
    public static ArrayList<ContentsNode> makeTreeData(List<ContentsNode> nodes){
        ArrayList<ContentsNode> tree = new ArrayList<>();
        ContentsNode node = null;
        for (ContentsNode child : nodes) {
            if(child.isHeading()) {
                if(node!=null) tree.add(node);
                node = child;
            } else {
                if(node!=null) node.addChild(child);
                else tree.add(child);
            }
        }
        if(node!=null) tree.add(node);
        return tree;
    }
    public boolean isHeading() {
        return isHeading;
    }

    public void setHeading(boolean heading) {
        isHeading = heading;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ArrayList<ContentsNode> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<ContentsNode> children) {
        this.children = children;
    }
}
