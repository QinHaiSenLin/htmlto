package com.xixudi.htmlto;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @desc jfinal doc 爬取并生成chm
 * @author YangZheng 328170112@qq.com
 * @date 2019-02-19 9:40
 */
public class JfinalDoc {

    private static String url="https://www.jfinal.com/doc";
    private static String baseUrl="https://www.jfinal.com";
    private static String projectName="jfinal";
    private static String basePath= JfinalDoc.class.getClassLoader().getResource(projectName).getPath();

    static class JfinalDownNode extends DownNode {
        JfinalDownNode(String url, String type, boolean cover) {
            super(url, type, cover);
        }

        JfinalDownNode(String url, String type) {
            super(url, type,false);
        }

        @Override
        protected String makeUrl() {
            if(url.startsWith("/")) {
                if(url.startsWith("//")) url = "http:"+url;
                else url = baseUrl+url;
            }
            return url;
        }

        @Override
        protected String makeNewFilePath() {
            return basePath + "/source/" + ("html".equals(type) ? "" : type + "/") + name;
        }
    }

    public static void downPage(String url) {
        try {
            JfinalDownNode page = new JfinalDownNode(url, "html",true);
            Document doc = page.getDoc();
            Elements csss = doc.select("link[href]");
            for (Element css : csss) {
                JfinalDownNode n = new JfinalDownNode(css.attr("href"), "css");
                n.saveToLocal();
                css.attr("href","./css/"+n.name);
            }
            Elements jss = doc.select("script[src]");
            for (Element js : jss) {
                JfinalDownNode n = new JfinalDownNode(js.attr("src"), "js");
                n.saveToLocal();
                js.attr("src","./js/"+n.name);
            }
            Elements imgs = doc.select("img[src]");
            for (Element img : imgs) {
                JfinalDownNode n = new JfinalDownNode(img.attr("src"), "img");
                n.saveToLocal();
                img.attr("src","./img/"+n.name);
            }
            doc.select(".jf-header-box,.jf-footer-box,.doc-menu-box,meta:not([http-equiv=\"content-type\"]),style:not([href])").remove();
            Element p = doc.select(".doc-content>p:last-child").get(0);
            if("<br>".equals(p.html())) p.remove();
            Elements menus = doc.select(".doc-pre-next-box a[href~=/doc/*]");
            for (Element a : menus) {
                a.attr("href",a.attr("href").replaceAll("^/doc",".")+".html");
            }
            Element title = doc.select(".doc-title").get(0);
            doc.title(title.text());
            page.saveDocToLocal();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static ArrayList<ContentsNode> downResource(){
        ArrayList<ContentsNode> nodes = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(url).get();
            Elements menus = doc.select(".doc-menu-box a");
            for (Element menu : menus) {
                String href = menu.attr("href");
                if(href.startsWith("/doc")) {
                    nodes.add(ContentsNode.newTopic(menu.text(),href.substring(5)+".html"));
                    downPage(href);
                } else {
                    nodes.add(ContentsNode.newHeading(menu.text()));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nodes;
    }
    public static void main(String[] args) {
        ArrayList<ContentsNode> contentsDownNodes = downResource();
        String today = new SimpleDateFormat("yyyyMMdd").format(new Date());
        MakeChm.createChm(projectName,contentsDownNodes,"index.html","Jfinal帮助文档@"+today,true);
    }

}